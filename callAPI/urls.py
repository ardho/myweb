from django.urls import path
from . import views

app_name = 'callAPI'

urlpatterns = [
    path('', views.index),
]
